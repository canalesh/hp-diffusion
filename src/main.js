import Vue from 'vue'
import App from './App.vue'
import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.css'

Vue.use(Vuetify)

Vue.filter('camelToHuman', function (value) {
  if (!value) return ''
  let words = value.match(/[A-Za-z][a-z]*/g)
  let capitalize = (word) => word.charAt(0).toUpperCase() + word.substring(1)
  return words.map(capitalize).join(' ')
})

new Vue({
  el: '#app',
  render: h => h(App)
})
