# HP Diffusion

This webapp calculates the the heat diffusion of material represented in a squared (N, N) simulation domain using the Laplacian operator and the diffusion coefficient of the material as follows:.

$
\begin{equation}
\frac{du}{dt} = D[\frac{d^2u}{d^2x} + \frac{d^2u}{d^2y}]
\end{equation}
$

where "u" is the temperature, "t" is time, "D" is the diffusion coefficient claculated from the thermal conductivity, heat capacity and density of the material, and "x" and "y" are the 2D dimensional positions.

![picture](public/example.PNG)
